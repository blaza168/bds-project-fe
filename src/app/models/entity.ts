export class ManualIocType {
  public name: string;
}

export class ManualIoc {
  public id: number;
  public idManualIocType: number;
  public value: string;
}
