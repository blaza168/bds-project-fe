import {ManualIoc} from './entity';

export class ManualIocTypeDetail {
  public manualIocType: string;
  public manualIocs: ManualIoc[];
}

export class ManualIocTypeStats {
  public iocType: string;
  public usageCount: number;
}

export class CreateResponse {
  public name: string;
}

export class UpdateResponse {
  public name: string;
}
