import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api-service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  public form: FormGroup;

  constructor(private api: ApiService, private router: Router) {
    this.form = this.buildForm();
  }

  ngOnInit(): void {
  }

  public onFormSubmit(): void {
    this.api.create(this.form.value.newName).subscribe((res) => {
      this.router.navigate(['/detail', res.name]);
    });
  }

  private buildForm(): FormGroup {
    return new FormGroup({
      newName: new FormControl(null, Validators.required),
    });
  }
}
