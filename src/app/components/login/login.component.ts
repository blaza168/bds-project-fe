import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api-service';
import {ManualIocTypeStats} from '../../models/response';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;
  public showError = false;
  public hidePasswd = false;

  constructor(private api: ApiService, private router: Router) {
    this.form = this.buildForm();
  }

  ngOnInit(): void {

  }

  public onFormSubmit(): void {
    this.api.login(this.form.value.username, this.form.value.password)
      .subscribe(() => {
        this.router.navigate(['/list']);
      }, (err) => {
        this.showError = true;
      });
  }

  private buildForm(): FormGroup {
    return new FormGroup({
      username: new FormControl('intern', Validators.required),
      password: new FormControl('solarwinds123', Validators.required),
    });
  }
}
