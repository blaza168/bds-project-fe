import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api-service';
import {ManualIocType} from '../../models/entity';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public manualIocTypes: ManualIocType[];

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.list().subscribe((data: ManualIocType[]) => {
      this.manualIocTypes = data;
    }, (err) => {
      console.error('unauthorized');
      this.router.navigate(['/']);
    });
  }

}
