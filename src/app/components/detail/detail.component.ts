import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../../services/api-service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {ManualIocTypeDetail, ManualIocTypeStats} from '../../models/response';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {

  private paramSub: Subscription;
  public manualIocTypeDetail: ManualIocTypeDetail;
  public manualIocTypeStats: ManualIocTypeStats;
  public filterModel: string;
  private currentName: string;

  constructor(private api: ApiService, route: ActivatedRoute, private router: Router) {
    this.paramSub = route.paramMap.subscribe((obj) => {
      this.currentName = obj['params']['name'];
      this.loadDetails(this.currentName);
    });
    this.filterModel = '';
  }

  private loadDetails(name): void {
    this.api.findByName(name)
      .subscribe((stats) => {
        this.manualIocTypeDetail = stats;
        console.log(stats);
      }, (err) => this.router.navigate(['/']));

    this.api.getStats(name).subscribe((obj) => {
      this.manualIocTypeStats = obj;
    });
  }

  public onDeleteClick(): void {
    this.api.delete(this.currentName).subscribe(() => {
      this.router.navigate(['/list']);
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.paramSub.unsubscribe();
  }

}
