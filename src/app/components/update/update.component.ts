import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../../services/api-service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public currentName: string;

  private paramSub: Subscription;

  constructor(private api: ApiService, private router: Router, route: ActivatedRoute) {
    this.paramSub = route.paramMap.subscribe((obj) => {
      this.currentName = obj['params']['name'];
    });
    this.form = this.buildForm();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.paramSub.unsubscribe();
  }

  public onFormSubmit(): void {
    this.api.update(this.currentName, this.form.value.newName).subscribe((res) => {
      this.router.navigate(['/detail', res.name]);
    });
  }

  private buildForm(): FormGroup {
    return new FormGroup({
      newName: new FormControl(null, Validators.required),
    });
  }

}
