import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api-service';

@Component({
  selector: 'app-sqli',
  templateUrl: './sqli.component.html',
  styleUrls: ['./sqli.component.scss']
})
export class SqliComponent implements OnInit {

  public model: string;
  public output: string[];

  constructor(private api: ApiService) {
    this.model = '\' UNION SELECT \'TEST\' --';
  }

  ngOnInit(): void {
  }

  public onBtnClick(): void {
    this.api.sqlInjection(this.model).subscribe((res) => {
      this.output = res;
    });
  }

}
