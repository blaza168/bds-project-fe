import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CreateResponse, ManualIocTypeDetail, ManualIocTypeStats, UpdateResponse} from '../models/response';
import {ManualIocType} from '../models/entity';
import {Injectable} from '@angular/core';

@Injectable()
export class ApiService {

  private static readonly BASE_URL = '/sedaq-rest/api/v1';

  constructor(public http: HttpClient) {
  }

  public update(old: string, newN: string): Observable<UpdateResponse> {
    return this.http.put<UpdateResponse>(ApiService.BASE_URL + '/manual-ioc-types', {
      currentName: old,
      newName: newN
    });
  }

  public create(name: string): Observable<CreateResponse> {
    return this.http.post<CreateResponse>(ApiService.BASE_URL + '/manual-ioc-types', {
      type: name
    });
  }

  public delete(name: string): Observable<void> {
    return this.http.delete<void>(ApiService.BASE_URL + '/manual-ioc-types/' + name);
  }

  public getStats(name: string): Observable<ManualIocTypeStats> {
    return this.http.get<ManualIocTypeStats>(ApiService.BASE_URL + '/manual-ioc-types/stats/' + name);
  }

  public list(): Observable<ManualIocType[]> {
    return this.http.get<ManualIocType[]>(ApiService.BASE_URL + '/manual-ioc-types');
  }

  public findByName(name: string): Observable<ManualIocTypeDetail> {
    return this.http.get<ManualIocTypeDetail>(ApiService.BASE_URL + '/manual-ioc-types/' + name);
  }

  public sqlInjection(p: string): Observable<string[]> {
    return this.http.post<string[]>(ApiService.BASE_URL + '/sqli', {
      payload: p
    });
  }

  public login(username: string, passwd: string): Observable<object> {
    const headersObj = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + passwd),
    'X-Requested-With': 'XMLHttpRequest'});

    return this.http.post<string[]>(ApiService.BASE_URL + '/sqli', {
      payload: 'random payload'
    }, {headers: headersObj});
  }
}
