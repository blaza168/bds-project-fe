import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {ListComponent} from './components/list/list.component';
import {DetailComponent} from './components/detail/detail.component';
import {UpdateComponent} from './components/update/update.component';
import {CreateComponent} from './components/create/create.component';
import {SqliComponent} from './components/sqli/sqli.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'list',
    component: ListComponent,
  },
  {
    path: 'detail/:name',
    component: DetailComponent,
  },
  {
    path: 'update/:name',
    component: UpdateComponent,
  },
  {
    path: 'create',
    component: CreateComponent,
  },
  {
    path: 'sqli',
    component: SqliComponent,
  },
  {
    path: '**',
    redirectTo: 'list',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
